package com.example.user.myapplication.data.models;

import com.example.user.myapplication.data.network_config.res.BookRes;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add a class header comment!
 */

public class Book {

	private String url;
	private String name;
	private List<String> authors = new ArrayList<>();
	private Integer numberOfPages;
	private String publisher;

	public Book(BookRes bookRes) {
		this.url = bookRes.getUrl();
		this.name = bookRes.getName();
		this.authors = bookRes.getAuthors();
		this.numberOfPages = bookRes.getNumberOfPages();
		this.publisher = bookRes.getPublisher();
	}

	public String getUrl() {
		return url;
	}

	public String getName() {
		return name;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public Integer getNumberOfPages() {
		return numberOfPages;
	}

	public String getPublisher() {
		return publisher;
	}
}
