package com.example.user.myapplication.screens.main.books;


import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.myapplication.R;
import com.example.user.myapplication.data.network_config.res.BookRes;
import com.example.user.myapplication.data.repository.RepositoryProvider;
import com.example.user.myapplication.screens.main.AbstractTabFragment;
import com.example.user.myapplication.utils.Const;
import com.example.user.myapplication.utils.Pagination.PaginationTool;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;

import static com.example.user.myapplication.utils.Const.KEY_RECYCLER_STATE_BOOKS;

public class BooksFragment extends AbstractTabFragment {

	protected BooksPagingRecyclerViewAdapter recyclerViewAdapter;

	public static BooksFragment newInstance(Context context) {
		BooksFragment fragment = new BooksFragment();
		fragment.setTitle(context.getString(R.string.menu_title_books));
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_houses, container, false);
		unbinder = ButterKnife.bind(this, view);
		init(savedInstanceState);
		return view;
	}

	private void init(Bundle savedInstanceState) {
		GridLayoutManager recyclerViewLayoutManager = new GridLayoutManager(getActivity(), 1);
		recyclerViewLayoutManager.supportsPredictiveItemAnimations();
		recyclerViewAdapter = new BooksPagingRecyclerViewAdapter();
		if (recyclerView != null) {
			recyclerView.setSaveEnabled(true);
			recyclerView.setLayoutManager(recyclerViewLayoutManager);
			recyclerView.setAdapter(recyclerViewAdapter);
		}




		if (savedInstanceState == null) {
			if (recyclerViewAdapter.isAllItemsLoaded()) {
				return;
			}
		}
		else {
			Log.d("TAG", "initreccc: " + savedInstanceState.toString());
			if (savedInstanceState.getBoolean(Const.KEY_ALL_ITEMS_LOADED_BOOKS)) {
				return;
			}
			Parcelable listState = savedInstanceState.getParcelable(KEY_RECYCLER_STATE_BOOKS);
			recyclerView.getLayoutManager().onRestoreInstanceState(listState);
			List<BookRes> gameEntities = savedInstanceState.getParcelableArrayList(Const.KEY_RECYCLER_ITEMS_BOOKS);
			if (gameEntities != null && gameEntities.size() > 0) {
				recyclerViewAdapter.addNewItems(gameEntities);
				recyclerViewAdapter.notifyItemInserted(gameEntities.size());
			}
		}

		// RecyclerView pagination
		PaginationTool<List<BookRes>> paginationTool = PaginationTool.buildPagingObservable(recyclerView, offset -> RepositoryProvider.provideGameApiRepository().getBooks(offset))
				.setLimit(Const.RECYCLER_LIMIT_PAGINATION_DEFAULT)
				.build();

		pagingSubscription = paginationTool
				.getPagingObservable()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(houseRes -> {
					recyclerViewAdapter.addNewItems(houseRes);
					recyclerViewAdapter.notifyItemInserted(recyclerViewAdapter.getItemCount() - houseRes.size());
				});
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(Const.KEY_ALL_ITEMS_LOADED_BOOKS, recyclerViewAdapter.isAllItemsLoaded());
		outState.putParcelableArrayList(Const.KEY_RECYCLER_ITEMS_BOOKS, (ArrayList<? extends Parcelable>) recyclerViewAdapter.getListElements());

		Parcelable listState = recyclerView.getLayoutManager().onSaveInstanceState();
		outState.putParcelable(Const.KEY_RECYCLER_STATE_BOOKS, listState);
	}
}
