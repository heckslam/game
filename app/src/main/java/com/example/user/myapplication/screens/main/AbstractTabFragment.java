package com.example.user.myapplication.screens.main;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import com.example.user.myapplication.R;

import butterknife.BindView;
import butterknife.Unbinder;
import rx.Subscription;


public abstract class AbstractTabFragment extends Fragment {
	private String title;
	protected Unbinder unbinder;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}


	@BindView(R.id.recycler)
	public RecyclerView recyclerView;

	protected Subscription pagingSubscription;

	@Override public void onDestroyView() {
		super.onDestroyView();
		if (unbinder != null) unbinder.unbind();
		if (pagingSubscription != null && !pagingSubscription.isUnsubscribed()) {
			pagingSubscription.unsubscribe();
		}

		// for memory leak prevention (RecycleView is not unsubscibed from adapter DataObserver)
		if (recyclerView != null) {
			recyclerView.setAdapter(null);
		}
	}
}
