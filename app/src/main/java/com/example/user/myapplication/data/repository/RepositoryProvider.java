package com.example.user.myapplication.data.repository;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;


public class RepositoryProvider {
	private static GameApiRepository sGameApiRepositoryRepository;
	private RepositoryProvider() {
	}
	@NonNull
	public static GameApiRepository provideGameApiRepository() {
		if (sGameApiRepositoryRepository == null) {
			sGameApiRepositoryRepository = new DefaultGameApiRepository();
		}
		return sGameApiRepositoryRepository;
	}

	@MainThread
	public static void init() {
		sGameApiRepositoryRepository = new DefaultGameApiRepository();
	}

}
