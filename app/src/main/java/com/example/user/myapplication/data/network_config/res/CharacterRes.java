package com.example.user.myapplication.data.network_config.res;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.user.myapplication.data.models.GameEntity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for character
 */

public class CharacterRes implements GameEntity{

	@SerializedName("url")
	@Expose
	private String url;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("gender")
	@Expose
	private String gender;
	@SerializedName("culture")
	@Expose
	private String culture;
	@SerializedName("born")
	@Expose
	private String born;
	@SerializedName("died")
	@Expose
	private String died;
	@SerializedName("titles")
	@Expose
	private List<String> titles = new ArrayList<>();
	@SerializedName("aliases")
	@Expose
	private List<String> aliases = new ArrayList<>();
	@SerializedName("father")
	@Expose
	private String father;
	@SerializedName("mother")
	@Expose
	private String mother;
	@SerializedName("spouse")
	@Expose
	private String spouse;
	@SerializedName("allegiances")
	@Expose
	private List<String> allegiances = new ArrayList<>();
	@SerializedName("books")
	@Expose
	private List<String> books = new ArrayList<>();
	@SerializedName("povBooks")
	@Expose
	private List<String> povBooks = new ArrayList<>();
	@SerializedName("tvSeries")
	@Expose
	private List<String> tvSeries = new ArrayList<>();
	@SerializedName("playedBy")
	@Expose
	private List<String> playedBy = new ArrayList<>();

	/**
	 *
	 * @return
	 * The url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 *
	 * @param url
	 * The url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 *
	 * @return
	 * The name
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 * The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 * The gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 *
	 * @param gender
	 * The gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 *
	 * @return
	 * The culture
	 */
	public String getCulture() {
		return culture;
	}

	/**
	 *
	 * @param culture
	 * The culture
	 */
	public void setCulture(String culture) {
		this.culture = culture;
	}

	/**
	 *
	 * @return
	 * The born
	 */
	public String getBorn() {
		return born;
	}

	/**
	 *
	 * @param born
	 * The born
	 */
	public void setBorn(String born) {
		this.born = born;
	}

	/**
	 *
	 * @return
	 * The died
	 */
	public String getDied() {
		return died;
	}

	/**
	 *
	 * @param died
	 * The died
	 */
	public void setDied(String died) {
		this.died = died;
	}

	/**
	 *
	 * @return
	 * The titles
	 */
	public List<String> getTitles() {
		return titles;
	}

	/**
	 *
	 * @param titles
	 * The titles
	 */
	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	/**
	 *
	 * @return
	 * The aliases
	 */
	public List<String> getAliases() {
		return aliases;
	}

	/**
	 *
	 * @param aliases
	 * The aliases
	 */
	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}

	/**
	 *
	 * @return
	 * The father
	 */
	public String getFather() {
		return father;
	}

	/**
	 *
	 * @param father
	 * The father
	 */
	public void setFather(String father) {
		this.father = father;
	}

	/**
	 *
	 * @return
	 * The mother
	 */
	public String getMother() {
		return mother;
	}

	/**
	 *
	 * @param mother
	 * The mother
	 */
	public void setMother(String mother) {
		this.mother = mother;
	}

	/**
	 *
	 * @return
	 * The spouse
	 */
	public String getSpouse() {
		return spouse;
	}

	/**
	 *
	 * @param spouse
	 * The spouse
	 */
	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	/**
	 *
	 * @return
	 * The allegiances
	 */
	public List<String> getAllegiances() {
		return allegiances;
	}

	/**
	 *
	 * @param allegiances
	 * The allegiances
	 */
	public void setAllegiances(List<String> allegiances) {
		this.allegiances = allegiances;
	}

	/**
	 *
	 * @return
	 * The books
	 */
	public List<String> getBooks() {
		return books;
	}

	/**
	 *
	 * @param books
	 * The books
	 */
	public void setBooks(List<String> books) {
		this.books = books;
	}

	/**
	 *
	 * @return
	 * The povBooks
	 */
	public List<String> getPovBooks() {
		return povBooks;
	}

	/**
	 *
	 * @param povBooks
	 * The povBooks
	 */
	public void setPovBooks(List<String> povBooks) {
		this.povBooks = povBooks;
	}

	/**
	 *
	 * @return
	 * The tvSeries
	 */
	public List<String> getTvSeries() {
		return tvSeries;
	}

	/**
	 *
	 * @param tvSeries
	 * The tvSeries
	 */
	public void setTvSeries(List<String> tvSeries) {
		this.tvSeries = tvSeries;
	}

	/**
	 *
	 * @return
	 * The playedBy
	 */
	public List<String> getPlayedBy() {
		return playedBy;
	}

	/**
	 *
	 * @param playedBy
	 * The playedBy
	 */
	public void setPlayedBy(List<String> playedBy) {
		this.playedBy = playedBy;
	}


	protected CharacterRes(Parcel in) {
		url = in.readString();
		name = in.readString();
		gender = in.readString();
		culture = in.readString();
		born = in.readString();
		died = in.readString();
		if (in.readByte() == 0x01) {
			titles = new ArrayList<String>();
			in.readList(titles, String.class.getClassLoader());
		} else {
			titles = null;
		}
		if (in.readByte() == 0x01) {
			aliases = new ArrayList<String>();
			in.readList(aliases, String.class.getClassLoader());
		} else {
			aliases = null;
		}
		father = in.readString();
		mother = in.readString();
		spouse = in.readString();
		if (in.readByte() == 0x01) {
			allegiances = new ArrayList<String>();
			in.readList(allegiances, String.class.getClassLoader());
		} else {
			allegiances = null;
		}
		if (in.readByte() == 0x01) {
			books = new ArrayList<String>();
			in.readList(books, String.class.getClassLoader());
		} else {
			books = null;
		}
		if (in.readByte() == 0x01) {
			povBooks = new ArrayList<String>();
			in.readList(povBooks, String.class.getClassLoader());
		} else {
			povBooks = null;
		}
		if (in.readByte() == 0x01) {
			tvSeries = new ArrayList<String>();
			in.readList(tvSeries, String.class.getClassLoader());
		} else {
			tvSeries = null;
		}
		if (in.readByte() == 0x01) {
			playedBy = new ArrayList<String>();
			in.readList(playedBy, String.class.getClassLoader());
		} else {
			playedBy = null;
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(url);
		dest.writeString(name);
		dest.writeString(gender);
		dest.writeString(culture);
		dest.writeString(born);
		dest.writeString(died);
		if (titles == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(titles);
		}
		if (aliases == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(aliases);
		}
		dest.writeString(father);
		dest.writeString(mother);
		dest.writeString(spouse);
		if (allegiances == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(allegiances);
		}
		if (books == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(books);
		}
		if (povBooks == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(povBooks);
		}
		if (tvSeries == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(tvSeries);
		}
		if (playedBy == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(playedBy);
		}
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<CharacterRes> CREATOR = new Parcelable.Creator<CharacterRes>() {
		@Override
		public CharacterRes createFromParcel(Parcel in) {
			return new CharacterRes(in);
		}

		@Override
		public CharacterRes[] newArray(int size) {
			return new CharacterRes[size];
		}
	};
}
