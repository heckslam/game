package com.example.user.myapplication.data.network_config.res;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.user.myapplication.data.models.GameEntity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 *  Class for book
 */

public class BookRes implements GameEntity {
	@SerializedName("url")
	@Expose
	private String url;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("isbn")
	@Expose
	private String isbn;
	@SerializedName("authors")
	@Expose
	private List<String> authors = new ArrayList<>();
	@SerializedName("numberOfPages")
	@Expose
	private Integer numberOfPages;
	@SerializedName("publisher")
	@Expose
	private String publisher;
	@SerializedName("country")
	@Expose
	private String country;
	@SerializedName("mediaType")
	@Expose
	private String mediaType;
	@SerializedName("released")
	@Expose
	private String released;
	@SerializedName("characters")
	@Expose
	private List<String> characters = new ArrayList<>();
	@SerializedName("povCharacters")
	@Expose
	private List<String> povCharacters = new ArrayList<>();

	/**
	 *
	 * @return
	 * The url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 *
	 * @param url
	 * The url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 *
	 * @return
	 * The name
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 * The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 * The isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 *
	 * @param isbn
	 * The isbn
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 *
	 * @return
	 * The authors
	 */
	public List<String> getAuthors() {
		return authors;
	}

	/**
	 *
	 * @param authors
	 * The authors
	 */
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	/**
	 *
	 * @return
	 * The numberOfPages
	 */
	public Integer getNumberOfPages() {
		return numberOfPages;
	}

	/**
	 *
	 * @param numberOfPages
	 * The numberOfPages
	 */
	public void setNumberOfPages(Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	/**
	 *
	 * @return
	 * The publisher
	 */
	public String getPublisher() {
		return publisher;
	}

	/**
	 *
	 * @param publisher
	 * The publisher
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	/**
	 *
	 * @return
	 * The country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 *
	 * @param country
	 * The country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 *
	 * @return
	 * The mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}

	/**
	 *
	 * @param mediaType
	 * The mediaType
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	/**
	 *
	 * @return
	 * The released
	 */
	public String getReleased() {
		return released;
	}

	/**
	 *
	 * @param released
	 * The released
	 */
	public void setReleased(String released) {
		this.released = released;
	}

	/**
	 *
	 * @return
	 * The characters
	 */
	public List<String> getCharacters() {
		return characters;
	}

	/**
	 *
	 * @param characters
	 * The characters
	 */
	public void setCharacters(List<String> characters) {
		this.characters = characters;
	}

	/**
	 *
	 * @return
	 * The povCharacters
	 */
	public List<String> getPovCharacters() {
		return povCharacters;
	}

	/**
	 *
	 * @param povCharacters
	 * The povCharacters
	 */
	public void setPovCharacters(List<String> povCharacters) {
		this.povCharacters = povCharacters;
	}

	protected BookRes(Parcel in) {
		url = in.readString();
		name = in.readString();
		isbn = in.readString();
		if (in.readByte() == 0x01) {
			authors = new ArrayList<String>();
			in.readList(authors, String.class.getClassLoader());
		} else {
			authors = null;
		}
		numberOfPages = in.readByte() == 0x00 ? null : in.readInt();
		publisher = in.readString();
		country = in.readString();
		mediaType = in.readString();
		released = in.readString();
		if (in.readByte() == 0x01) {
			characters = new ArrayList<String>();
			in.readList(characters, String.class.getClassLoader());
		} else {
			characters = null;
		}
		if (in.readByte() == 0x01) {
			povCharacters = new ArrayList<String>();
			in.readList(povCharacters, String.class.getClassLoader());
		} else {
			povCharacters = null;
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(url);
		dest.writeString(name);
		dest.writeString(isbn);
		if (authors == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(authors);
		}
		if (numberOfPages == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(numberOfPages);
		}
		dest.writeString(publisher);
		dest.writeString(country);
		dest.writeString(mediaType);
		dest.writeString(released);
		if (characters == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(characters);
		}
		if (povCharacters == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeList(povCharacters);
		}
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<BookRes> CREATOR = new Parcelable.Creator<BookRes>() {
		@Override
		public BookRes createFromParcel(Parcel in) {
			return new BookRes(in);
		}

		@Override
		public BookRes[] newArray(int size) {
			return new BookRes[size];
		}
	};
}
