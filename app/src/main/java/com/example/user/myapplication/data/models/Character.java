package com.example.user.myapplication.data.models;

import com.example.user.myapplication.data.network_config.res.CharacterRes;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add a class header comment!
 */

public class Character {
	private String url;
	private String name;
	private String gender;
	private String culture;
	private String born;
	private String died;
	private List<String> titles = new ArrayList<>();
	private List<String> aliases = new ArrayList<>();
	private String father;
	private String mother;
	private String spouse;
	private List<String> allegiances = new ArrayList<>();
	private List<String> books = new ArrayList<>();
	private List<String> povBooks = new ArrayList<>();
	private List<String> tvSeries = new ArrayList<>();
	private List<String> playedBy = new ArrayList<>();

	public Character(CharacterRes characterRes) {
		this.url = characterRes.getUrl();
		this.name = characterRes.getName();
		this.gender = characterRes.getGender();
		this.culture = characterRes.getCulture();
		this.born = characterRes.getBorn();
		this.died = characterRes.getDied();
		this.titles = characterRes.getTitles();
		this.aliases = characterRes.getAliases();
		this.father = characterRes.getFather();
		this.mother = characterRes.getMother();
		this.spouse = characterRes.getSpouse();
		this.allegiances = characterRes.getAllegiances();
		this.books = characterRes.getBooks();
		this.povBooks = characterRes.getPovBooks();
		this.tvSeries = characterRes.getTvSeries();
		this.playedBy = characterRes.getPlayedBy();
	}

	public String getUrl() {
		return url;
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public String getCulture() {
		return culture;
	}

	public String getBorn() {
		return born;
	}

	public String getDied() {
		return died;
	}

	public List<String> getTitles() {
		return titles;
	}

	public List<String> getAliases() {
		return aliases;
	}

	public String getFather() {
		return father;
	}

	public String getMother() {
		return mother;
	}

	public String getSpouse() {
		return spouse;
	}

	public List<String> getAllegiances() {
		return allegiances;
	}

	public List<String> getBooks() {
		return books;
	}

	public List<String> getPovBooks() {
		return povBooks;
	}

	public List<String> getTvSeries() {
		return tvSeries;
	}

	public List<String> getPlayedBy() {
		return playedBy;
	}
}
