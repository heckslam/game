package com.example.user.myapplication.utils.Pagination;


class PagingException extends RuntimeException {
	PagingException(String detailMessage) {
		super(detailMessage);
	}
}
