package com.example.user.myapplication.screens.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.user.myapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * HOLDER FOR RECYCLER ITEMS
 */

public class RecyclerItemViewHolder extends RecyclerView.ViewHolder {
	@BindView(R.id.name) public TextView name;
	@BindView(R.id.desc) public TextView desc;
	@BindView(R.id.region) public TextView region;
	public RecyclerItemViewHolder(View itemView) {
		super(itemView);
		ButterKnife.bind(this, itemView);
	}
}
