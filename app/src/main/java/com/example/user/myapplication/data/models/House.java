package com.example.user.myapplication.data.models;

import com.example.user.myapplication.data.network_config.res.HouseRes;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add a class header comment!
 */

public class House {
	private String url;
	private String name;
	private String region;
	private String coatOfArms;
	private String words;
	private List<String> titles = new ArrayList<>();
	private List<String> seats = new ArrayList<>();
	private String currentLord;
	private String heir;
	private String overlord;
	private String founded;
	private String founder;
	private String diedOut;
	private List<String> ancestralWeapons = new ArrayList<>();
	private List<String> cadetBranches = new ArrayList<>();
	private List<String> swornMembers = new ArrayList<>();

	public House(HouseRes houseRes) {
		this.url = houseRes.getUrl();
		this.name = houseRes.getName();
		this.region = houseRes.getRegion();
		this.coatOfArms = houseRes.getCoatOfArms();
		this.words = houseRes.getWords();
		this.titles = houseRes.getTitles();
		this.seats = houseRes.getSeats();
		this.currentLord = houseRes.getCurrentLord();
		this.heir = houseRes.getHeir();
		this.overlord = houseRes.getOverlord();
		this.founded = houseRes.getFounded();
		this.founder = houseRes.getFounder();
		this.diedOut = houseRes.getDiedOut();
		this.ancestralWeapons = houseRes.getAncestralWeapons();
		this.cadetBranches = houseRes.getCadetBranches();
		this.swornMembers = houseRes.getSwornMembers();
	}

	public String getUrl() {
		return url;
	}

	public String getName() {
		return name;
	}

	public String getRegion() {
		return region;
	}

	public String getCoatOfArms() {
		return coatOfArms;
	}

	public String getWords() {
		return words;
	}

	public List<String> getTitles() {
		return titles;
	}

	public List<String> getSeats() {
		return seats;
	}

	public String getCurrentLord() {
		return currentLord;
	}

	public String getHeir() {
		return heir;
	}

	public String getOverlord() {
		return overlord;
	}

	public String getFounded() {
		return founded;
	}

	public String getFounder() {
		return founder;
	}

	public String getDiedOut() {
		return diedOut;
	}

	public List<String> getAncestralWeapons() {
		return ancestralWeapons;
	}

	public List<String> getCadetBranches() {
		return cadetBranches;
	}

	public List<String> getSwornMembers() {
		return swornMembers;
	}
}
