package com.example.user.myapplication.utils.Pagination;


import rx.Observable;

public interface PagingListener<T> {
	Observable<T> onNextPage(int offset);
}
