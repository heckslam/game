package com.example.user.myapplication.screens.main.characters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.myapplication.R;
import com.example.user.myapplication.data.network_config.res.CharacterRes;
import com.example.user.myapplication.data.network_config.res.HouseRes;
import com.example.user.myapplication.screens.main.RecyclerItemViewHolder;

import java.util.ArrayList;
import java.util.List;

public class CharactersPagingRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerItemViewHolder> {

	private List<CharacterRes> listElements = new ArrayList<>();

	private boolean allItemsLoaded;

	public void addNewItems(List<CharacterRes> items) {
		if (items.size() == 0) {
			allItemsLoaded = true;
			return;
		}
		listElements.addAll(items);
	}

	public List<CharacterRes> getListElements() {
		return listElements;
	}

	public boolean isAllItemsLoaded() {
		return allItemsLoaded;
	}

	public CharacterRes getItem(int position) {
		return listElements.get(position);
	}

	@Override
	public int getItemCount() {
		return listElements.size();
	}

	@Override
	public RecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_house_item, parent, false);
		return new RecyclerItemViewHolder(v);
	}

	@Override
	public void onBindViewHolder(RecyclerItemViewHolder holder, int position) {
		holder.name.setText(getItem(position).getName());
		holder.region.setText(getItem(position).getGender());
		holder.desc.setText((getItem(position).getAliases().toString()));
	}

}
