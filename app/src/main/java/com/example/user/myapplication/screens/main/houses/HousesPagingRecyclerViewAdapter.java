package com.example.user.myapplication.screens.main.houses;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.myapplication.R;
import com.example.user.myapplication.data.network_config.res.BookRes;
import com.example.user.myapplication.data.network_config.res.CharacterRes;
import com.example.user.myapplication.data.network_config.res.HouseRes;
import com.example.user.myapplication.screens.main.RecyclerItemViewHolder;

import java.util.ArrayList;
import java.util.List;

public class HousesPagingRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerItemViewHolder> {

	private List<HouseRes> listElements = new ArrayList<>();

	private boolean allItemsLoaded;

	public void addNewItems(List<HouseRes> items) {
		if (items.size() == 0) {
			allItemsLoaded = true;
			return;
		}
		listElements.addAll(items);
	}

	public List<HouseRes> getListElements() {
		return listElements;
	}

	public boolean isAllItemsLoaded() {
		return allItemsLoaded;
	}

	public HouseRes getItem(int position) {
		return listElements.get(position);
	}

	@Override
	public int getItemCount() {
		return listElements.size();
	}

	@Override
	public RecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_house_item, parent, false);
		return new RecyclerItemViewHolder(v);
	}

	@Override
	public void onBindViewHolder(RecyclerItemViewHolder holder, int position) {
		if (getItem(position) != null) {
			holder.name.setText((getItem(position)).getName());
			holder.region.setText((getItem(position)).getRegion());
			holder.desc.setText((getItem(position)).getCoatOfArms());
		}

	}

}
