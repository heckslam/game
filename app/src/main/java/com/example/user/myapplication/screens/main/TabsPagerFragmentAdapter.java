package com.example.user.myapplication.screens.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.example.user.myapplication.screens.main.books.BooksFragment;
import com.example.user.myapplication.screens.main.characters.CharactersFragment;
import com.example.user.myapplication.screens.main.houses.HousesFragment;
import com.example.user.myapplication.utils.Const;


public class TabsPagerFragmentAdapter extends FragmentStatePagerAdapter {

	private SparseArray<AbstractTabFragment> tabs;
	public TabsPagerFragmentAdapter(Context context, FragmentManager fm) {
		super(fm);
		initTabsMap(context);
	}

	@Override
	public Fragment getItem(int position) {
		return tabs.get(position);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabs.get(position).getTitle();
	}

	@Override
	public int getCount() {
		return tabs.size();
	}

	private void initTabsMap(Context context) {
		tabs = new SparseArray<>();
		tabs.put(Const.TAB_HOUSES, HousesFragment.newInstance(context));
		tabs.put(Const.TAB_CHARACTERS, CharactersFragment.newInstance(context));
		tabs.put(Const.TAB_BOOKS, BooksFragment.newInstance(context));
	}
}
