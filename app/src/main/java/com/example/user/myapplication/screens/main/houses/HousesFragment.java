package com.example.user.myapplication.screens.main.houses;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.myapplication.R;
import com.example.user.myapplication.data.network_config.res.HouseRes;
import com.example.user.myapplication.data.repository.RepositoryProvider;
import com.example.user.myapplication.screens.main.AbstractTabFragment;
import com.example.user.myapplication.utils.Const;
import com.example.user.myapplication.utils.Pagination.PaginationTool;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;

import static com.example.user.myapplication.utils.Const.KEY_RECYCLER_STATE_HOUSE;

public class HousesFragment extends AbstractTabFragment {


	HousesPagingRecyclerViewAdapter recyclerViewAdapter;

	public static HousesFragment newInstance(Context context) {
		HousesFragment fragment = new HousesFragment();
		fragment.setTitle(context.getString(R.string.menu_title_houses));
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_houses, container, false);
		unbinder = ButterKnife.bind(this, view);
		init(savedInstanceState);

		return view;
	}


	private void init( Bundle savedInstanceState) {
		GridLayoutManager recyclerViewLayoutManager = new GridLayoutManager(getActivity(), 1);
		recyclerViewLayoutManager.supportsPredictiveItemAnimations();
		recyclerViewAdapter = new HousesPagingRecyclerViewAdapter();
		if (recyclerView != null) {
			recyclerView.setSaveEnabled(true);
			recyclerView.setLayoutManager(recyclerViewLayoutManager);
			recyclerView.setAdapter(recyclerViewAdapter);
		}



		if (savedInstanceState == null) {
			if (recyclerViewAdapter.isAllItemsLoaded()) {
				return;
			}
		}
		else {
			if (savedInstanceState.getBoolean(Const.KEY_ALL_ITEMS_LOADED_HOUSE)) {
				return;
			}
			Parcelable listState = savedInstanceState.getParcelable(KEY_RECYCLER_STATE_HOUSE);
			recyclerView.getLayoutManager().onRestoreInstanceState(listState);
			List<HouseRes> gameEntities = savedInstanceState.getParcelableArrayList(Const.KEY_RECYCLER_ITEMS_HOUSE);
			if (gameEntities != null && gameEntities.size() > 0) {
				recyclerViewAdapter.addNewItems(gameEntities);
				recyclerViewAdapter.notifyItemInserted(gameEntities.size());
			}
		}


		// RecyclerView pagination
		PaginationTool<List<HouseRes>> paginationTool = PaginationTool.buildPagingObservable(recyclerView, offset -> RepositoryProvider.provideGameApiRepository().getHouses(offset))
				.setLimit(Const.RECYCLER_LIMIT_PAGINATION_DEFAULT)
				.build();

		pagingSubscription = paginationTool
				.getPagingObservable()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(houseRes -> {
					recyclerViewAdapter.addNewItems(houseRes);
					recyclerViewAdapter.notifyItemInserted(recyclerViewAdapter.getItemCount() - houseRes.size());
				});
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(Const.KEY_ALL_ITEMS_LOADED_HOUSE, recyclerViewAdapter.isAllItemsLoaded());
		outState.putParcelableArrayList(Const.KEY_RECYCLER_ITEMS_HOUSE, (ArrayList<? extends Parcelable>) recyclerViewAdapter.getListElements());

		Parcelable listState = recyclerView.getLayoutManager().onSaveInstanceState();
		outState.putParcelable(Const.KEY_RECYCLER_STATE_HOUSE, listState);
	}


}
