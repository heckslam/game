package com.example.user.myapplication.data.network_config;

import android.support.annotation.NonNull;

import com.example.user.myapplication.utils.Const;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ServiceFactory {
	private static OkHttpClient sClient;

	private static volatile ApiService sService;

	private ServiceFactory() {}


	@NonNull
	public static ApiService getGameApiService() {
		ApiService service = sService;
		if (service == null) {
			synchronized (ServiceFactory.class) {
				service = sService;
				if (service == null) {
					service = sService = buildRetrofit().create(ApiService.class);
				}
			}
		}
		return service;
	}

	public static void recreate() {
		sClient = null;
		sClient = getClient();
		sService = buildRetrofit().create(ApiService.class);
	}


	@NonNull
	private static Retrofit buildRetrofit() {
		return new Retrofit.Builder()
				.baseUrl(Const.BASE_URL)
				.client(getClient())
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.build();
	}

	@NonNull
	private static OkHttpClient getClient() {
		OkHttpClient client = sClient;
		if (client == null) {
			synchronized (ServiceFactory.class) {
				client = sClient;
				if (client == null) {
					client = sClient = buildClient();
				}
			}
		}
		return client;
	}

	@NonNull
	private static OkHttpClient buildClient() {
		HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
		loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		return new OkHttpClient.Builder()
				.addInterceptor(loggingInterceptor)
				.addInterceptor(new StethoInterceptor())
				.build();
	}
}
