package com.example.user.myapplication.data.repository;

import android.support.annotation.NonNull;

import com.example.user.myapplication.data.network_config.res.BookRes;
import com.example.user.myapplication.data.network_config.res.CharacterRes;
import com.example.user.myapplication.data.network_config.res.HouseRes;

import java.util.List;

import rx.Observable;



public interface GameApiRepository {
	@NonNull
	Observable<List<HouseRes>> getHouses(int offset);

	@NonNull
	Observable<List<CharacterRes>> getCharacters(int offset);

	@NonNull
	Observable<List<BookRes>> getBooks(int offset);
}
