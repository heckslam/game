package com.example.user.myapplication.utils;


public class Utils {
    public static int getPage(int offset) {
        if (offset > 0 && offset % 10 == 0) {
            return (offset / (Const.RECYCLER_LIMIT_PAGINATION_DEFAULT - 1)) + 1;
        }
        else if (offset > 0) {
            return 0;
        }
        else  {
            return 1;
        }
    }
}
