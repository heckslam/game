package com.example.user.myapplication.data.repository;

import android.support.annotation.NonNull;

import com.example.user.myapplication.data.models.Book;
import com.example.user.myapplication.data.models.Character;
import com.example.user.myapplication.data.models.House;
import com.example.user.myapplication.data.network_config.ServiceFactory;
import com.example.user.myapplication.data.network_config.res.BookRes;
import com.example.user.myapplication.data.network_config.res.CharacterRes;
import com.example.user.myapplication.data.network_config.res.HouseRes;
import com.example.user.myapplication.utils.Const;
import com.example.user.myapplication.utils.RxUtils;
import com.example.user.myapplication.utils.Utils;

import java.util.List;

import rx.Observable;



public class DefaultGameApiRepository implements GameApiRepository {
	@NonNull
	@Override
	public Observable<List<HouseRes>> getHouses(int offset) {
		int page = Utils.getPage(offset);
		if (page != 0)
			return ServiceFactory.getGameApiService().getHouses(Const.RECYCLER_LIMIT_PAGINATION_DEFAULT, page)
					.cache()
					.compose(RxUtils.async());
		else return Observable.empty();
	}

	@NonNull
	@Override
	public Observable<List<CharacterRes>> getCharacters(int offset) {
		int page = Utils.getPage(offset);
		if (page != 0)
			return ServiceFactory.getGameApiService().getCharacters(Const.RECYCLER_LIMIT_PAGINATION_DEFAULT, page)
					.cache()
					.compose(RxUtils.async());
		else return Observable.empty();
	}

	@NonNull
	@Override
	public Observable<List<BookRes>> getBooks(int offset) {
		int page = Utils.getPage(offset);
		if (page != 0)
			return ServiceFactory.getGameApiService().getBooks(Const.RECYCLER_LIMIT_PAGINATION_DEFAULT, page)
					.cache()
					.compose(RxUtils.async());
		else return Observable.empty();
	}
}
