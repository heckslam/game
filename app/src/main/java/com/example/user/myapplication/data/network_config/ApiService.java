package com.example.user.myapplication.data.network_config;


import com.example.user.myapplication.data.network_config.res.BookRes;
import com.example.user.myapplication.data.network_config.res.CharacterRes;
import com.example.user.myapplication.data.network_config.res.HouseRes;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {
	//Api HOUSES
	@GET("/api/houses/")
	Observable<List<HouseRes>> getHouses(@Query("pageSize") int limit, @Query("page") int offset);

	//Api CHARACTERS
	@GET("/api/characters/")
	Observable<List<CharacterRes>> getCharacters(@Query("pageSize") int limit, @Query("page") int offset);

	//Api BOOKS
	@GET("/api/books/")
	Observable<List<BookRes>> getBooks(@Query("pageSize") int limit, @Query("page") int offset);
}
