package com.example.user.myapplication.screens.main;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.user.myapplication.R;
import com.example.user.myapplication.screens.main.TabsPagerFragmentAdapter;
import com.example.user.myapplication.utils.Const;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

	@BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
	@BindView(R.id.main_pager) ViewPager viewPager;
	@BindView(R.id.toolbar) Toolbar mToolbar;
	@BindView(R.id.navigation) NavigationView mNavigationView;
	@BindView(R.id.tabs) TabLayout mTabLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		initUI();
	}

	private void initUI() {
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_navigation, R.string.close_navigation);
		mDrawerLayout.addDrawerListener(toggle);
		toggle.syncState();

		TabsPagerFragmentAdapter adapter = new TabsPagerFragmentAdapter(this, getSupportFragmentManager());
		viewPager.setAdapter(adapter);

		mTabLayout.setupWithViewPager(viewPager);

		mNavigationView.setNavigationItemSelectedListener(item -> {
			mDrawerLayout.closeDrawers();
			switch (item.getItemId()) {
				case R.id.houses:
					viewPager.setCurrentItem(Const.TAB_HOUSES);
					break;
				case R.id.characters:
					viewPager.setCurrentItem(Const.TAB_CHARACTERS);
					break;
				case R.id.books:
					viewPager.setCurrentItem(Const.TAB_BOOKS);
					break;
			}
			return true;
		});
	}
}
